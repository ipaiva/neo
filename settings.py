import logging
from configparser import ConfigParser

#Read config.ini file
config = ConfigParser()
config.read("_config/config.ini")

class Config:
    userinfo = config["USER"]
    app = config["APP"]
    server = config["SERVERCONFIG"]
    dev = config['DEV']
    paths = config['PATHS']
    db = config['DB']

logging.basicConfig(filename=Config.paths['log'] + 'neo.log', level=logging.INFO, format='%(asctime)s %(message)s')

phrases = ['Esse BO não é meu', 'Not my business', 'Foda-se', 'Essa pica é do aspira', 'Comigo não morreu', 'Nunca nem vi']