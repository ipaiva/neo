import pandas as pd
import adal
import requests
import system
import time
import json
import datetime
import settings
import random
from entity import *

cfg = settings.Config
log = settings.logging
phrases = settings.phrases

def test(id, param=0):
    try:
        notify_msg = "[dev] Discord Integration test via webooks"
        service = 'all'
        # notify_msg = "{date} - {msg}".format(date=datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S") , msg=msg)
        print(notify_msg)

        slackMsg = json.dumps({"text": notify_msg})
        url_slack = 'https://hooks.slack.com/services/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'

        tgramMsg = notify_msg
        url_tgram = 'https://api.telegram.org/bot1047135654:AAFwhNOT0c6koKwEkc-JaSQQDMxbTapwnZI/sendMessage?chat_id=-1001401945730_17997831658809190542&text={msg}'.format(msg=tgramMsg)

        discordMsg = {"content": notify_msg}
        url_discord = 'https://discord.com/api/webhooks/862773135244918794/eLd4WI4gf08S1cyiQH8GlEnugGi4Z7qzb1ALG8uR_bu0zhzR9ewHNOsXG9ipo86SDx6l'

        if service == 'slack':
            response = requests.post(url_slack, data = slackMsg)
        elif service == 'tgram':
            response = requests.post(url_tgram)
        elif service == 'discord':
            response = requests.post(url_discord, json = discordMsg)
        else:
            response = requests.post(url_slack, data = slackMsg)
            response = requests.post(url_tgram)
            response = requests.post(url_discord, json = discordMsg)

        print(response.status_code)

        print(response.content)


        # date = datetime.datetime.now()
        # date = date.strftime("%Y%m%d")
        # file_name = 'Get_Refreshables_For_Capacity_'+date+'.json'
        # save_json("", file_name, cfg.paths['output'] + "Refreshables/")

        # df_report_dataset = pd.read_csv("input/reports_datasets_to_be_removed.csv")
        # ls_group_report_dataset = df_report_dataset.values.tolist()

        # for row in ls_group_report_dataset:
        #     groupId = row[0]
        #     reportId = row[1]
        #     datasetId = row[2]

        #     print(groupId)
        #     print(reportId)
        #     print(datasetId)


        # df_report_dataset = pd.read_csv("input/reports_datasets_to_be_removed.csv")

        # report_dataset_id = id

        # groupId = report_dataset_id.split("|")[0]
        # reportId = report_dataset_id.split("|")[1]
        # datasetId = report_dataset_id.split("|")[2]

        # print("Workspace: " + groupId + "\n")
        # print("Report: " + reportId + "\n")
        # print("Dataset: " + datasetId + "\n")

        # print(rows)

        # print(param)
        # print(Dataset[id].name)

        # if (Dataset[id].name == 'CORPORATIVO' and param == '1'):
        #     print("Hello world!")
        # print(Dataset[id].name)
        # print(Dataset[id].value)

        # for ds in Dataset:
        #     print(ds.name)
        # notify('[DEV] - Notification test')

        # ws = Workspace()
        # print(var_aux)
        # content = b'{ no content }'
        # file_name = 'response.json'
        # save_json(content, file_name, cfg.paths['output'] + "Test/")

        # notify('Notification test')
    except Exception as e:
        print(e)
        log.exception(e)

def get_refreshables():
    log.info("Method Get Refreshables")
    token = get_token(cfg.server['auth_method'])
    url = 'https://api.powerbi.com/v1.0/myorg/admin/capacities/refreshables?%24top=100000'

    date = datetime.datetime.now().strftime("%Y%m%d")
    file_name = 'Get_Refreshables_For_Capacity_'+date+'.json'

    response = requests.get(url, headers={'Authorization' : 'Bearer ' + token})
    save_json(response.content, file_name, cfg.paths['output'] + "Refreshables/")

def get_group_users():
    log.info("Method Get Group Users")
    token = get_token(cfg.server['auth_method'])
    if(cfg.dev['debug'] == '1'):
        df_groups = pd.read_csv("input/workspaces_sample.csv")
    else:
        df_groups = pd.read_csv("input/workspaces.csv")
    ls_groups = df_groups['GROUP_ID'].tolist()
    ls_empty_workspaces = []

    if(cfg.dev['debug'] == '1'):
        print(group_list)

    for groupId in ls_groups:
        url = "https://api.powerbi.com/v1.0/myorg/groups/" + groupId + "/users"
        response = requests.get(url, headers={'Authorization' : 'Bearer ' + token})
        file_name = "workspace_users_" + groupId + ".csv"
        full_path = cfg.paths['output'] + "Group Users/"

        try:
            df_response = pd.read_json(response.content)
            save_csv(df_response, file_name, full_path)
        except:
            ls_empty_workspaces.append(file_name)

    df_empty_workspaces = pd.DataFrame(ls_empty_workspaces)
    df_empty_workspaces.to_csv(full_path + 'empty_workspaces.csv', index=False)

def refresh_dataset(id, check=0):
    try:
        log.info("Method Refresh Dataset")
        token = get_token(cfg.server['auth_method'])
        groupId = Workspace.PBI_OFICIAL.value
        file_name = 'Refresh_dataset.json'
        datasetId = Dataset[id].value
        url = 'https://api.powerbi.com/v1.0/myorg/groups/' + groupId + '/datasets/' + datasetId + '/refreshes'

        if(cfg.dev['debug'] == '0'):
            notify('Painel ' + Dataset[id].name + ' atualização INICIADA')
            response = requests.post(url, headers={'Authorization' : 'Bearer ' + token})
            save_json(response.content, file_name, cfg.paths['output'])
        elif(cfg.dev['debug'] == '1'):
            notify('[DEV] Painel ' + Dataset[id].name + ' atualização INICIADA')

        if (Dataset[id].name == 'CORPORATIVO' and check == '1'):
            check_databases(refresh=1)
    except Exception as e:
        print(e)
        log.exception(e)

def save_json(content, file, path=cfg.paths['output']):
    log.info("Method Save Json")
    print('\rSaving ' + file + ' to ' + path)
    with open(path + file, 'wb') as f:
        f.write(content)
    print('JSON saved at ' + path)

def save_csv(content, file, path):
    content.to_csv(path + file)
    print('CSV saved at ' + path)

def delete_workspace(ws_id = ''):
    log.info("Method Delete workspace")
    token = get_token(cfg.server['auth_method'])
    file_name = 'delete_workspace.json'
    
    if not ws_id:
        df_groups = pd.read_csv("input/datasets_to_be_removed.csv")
        ls_groups = df_groups['GROUP_ID'].tolist()
        for groupId in ls_groups:
            print("deleting " + groupId)
            url = "https://api.powerbi.com/v1.0/myorg/groups/"
            file_name = 'delete_workspace_' + groupId + '.json'
            url = url + groupId
            response = requests.delete(url, headers={'Authorization' : 'Bearer ' + token})
            save_json(response.content, file_name, cfg.paths['output'] + "Delete/")
    else:
        url = "https://api.powerbi.com/v1.0/myorg/groups/" + ws_id
        response = requests.delete(url, headers={'Authorization' : 'Bearer ' + token})
        save_json(response.content, file_name, cfg.paths['output'] + "Delete/")

def delete_report_dataset(report_dataset_id = ''):
    log.info("Method delete report and dataset")
    token = get_token(cfg.server['auth_method'])
    file_name = 'delete_report_dataset.json'

    if not report_dataset_id:
        df_report_dataset = pd.read_csv("input/reports_datasets_to_be_removed.csv")
        ls_group_report_dataset = df_report_dataset.values.tolist()

        for row in ls_group_report_dataset:
            groupId = row[0]
            reportId = row[1]
            datasetId = row[2]

            print("deleting Workspace:" + groupId + " - Report: " + reportId + " - Dataset: " + datasetId)
            log.info("deleting Workspace:" + groupId + " - Report: " + reportId + " - Dataset: " + datasetId)
            url = "https://api.powerbi.com/v1.0/myorg/groups/" + groupId + "/reports/" + reportId
            file_name = 'delete_report_' + reportId + '.json'
            response = requests.delete(url, headers={'Authorization' : 'Bearer ' + token})
            save_json(response.content, file_name, cfg.paths['output'] + "Delete/")

            url = "https://api.powerbi.com/v1.0/myorg/groups/" + groupId + "/datasets/" + datasetId
            file_name = 'delete_dataset_' + datasetId + '.json'
            response = requests.delete(url, headers={'Authorization' : 'Bearer ' + token})
            save_json(response.content, file_name, cfg.paths['output'] + "Delete/")

    else:
        groupId = report_dataset_id.split("|")[0]
        reportId = report_dataset_id.split("|")[1]
        datasetId = report_dataset_id.split("|")[2]

        print("deleting Workspace:" + groupId + " - Report: " + reportId + " - Dataset: " + datasetId)
        log.info("deleting Workspace:" + groupId + " - Report: " + reportId + " - Dataset: " + datasetId)
        url = "https://api.powerbi.com/v1.0/myorg/groups/" + groupId + "/reports/" + reportId
        file_name = 'delete_report_' + reportId + '.json'
        response = requests.delete(url, headers={'Authorization' : 'Bearer ' + token})
        save_json(response.content, file_name, cfg.paths['output'] + "Delete/")

        print("deleting " + datasetId)
        url = "https://api.powerbi.com/v1.0/myorg/groups/{groupId}/datasets/" + datasetId
        file_name = 'delete_dataset_' + datasetId + '.json'
        response = requests.delete(url, headers={'Authorization' : 'Bearer ' + token})
        save_json(response.content, file_name, cfg.paths['output'] + "Delete/")

def get_refreshables_by_capacity():
    token = get_token(cfg.server['auth_method'])
    capacityId = '1A3AB113-8B84-4471-9303-1A5A0B911123'
    url = 'https://api.powerbi.com/v1.0/myorg/admin/capacities/' + capacityId + '/refreshables'
    return requests.get(url, headers={'Authorization' : 'Bearer ' + token})

def print_response(response, content = 0):
    print('Response status: ' + str(response.status_code))

    if(cfg.dev['debug'] == '1' | content == 1):
        print('Response content: ')
        print(response.content)

def notify(notify_msg, service='all'):
    # notify_msg = "{date} - {msg}".format(date=datetime.datetime.now().strftime("%Y%m%d %H%M%S") , msg=msg)

    slackMsg = {"text": notify_msg}
    slackMsg = json.dumps(slackMsg)
    url_slack = 'https://hooks.slack.com/services/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'

    tgramMsg = notify_msg
    url_tgram = 'https://api.telegram.org/bot1047135654:AAFwhNOT0c6koKwEkc-JaSQQDMxbTapwnZI/sendMessage?chat_id=-1001401945730_17997831658809190542&text={msg}'.format(msg=tgramMsg)

    discordMsg = {"content": notify_msg}
    url_discord = 'https://discord.com/api/webhooks/862773135244918794/eLd4WI4gf08S1cyiQH8GlEnugGi4Z7qzb1ALG8uR_bu0zhzR9ewHNOsXG9ipo86SDx6l'

    if service == 'slack':
        response = requests.post(url_slack, data = slackMsg)
    elif service == 'tgram':
        response = requests.post(url_tgram)
    elif service == 'discord':
        response = requests.post(url_discord, json = discordMsg)
    else:
        response = requests.post(url_slack, data = slackMsg)
        response = requests.post(url_tgram)
        response = requests.post(url_discord, json = discordMsg)

def check_databases(refresh = 0):
    log.info("Method Check Database")
    conn = system.connect_oracle()
    ciclo = '20212'

    d1, d2 = get_datas_captacao(ciclo, conn)
    df_captacao_analitico_d1 = get_captacao_analitico(ciclo, d1, conn)
    df_captacao_analitico_d2 = get_captacao_analitico(ciclo, d2, conn)
    df_group_grfcs_d1 = get_group_graficos(ciclo, d1, conn)
    df_fato_d1 = get_fato(ciclo, d1, conn)
    df_fato_d2 = get_fato(ciclo, d2, conn)

    system.close_oracle(conn)
    result_group, error_group = validate_group(df_captacao_analitico_d1, df_group_grfcs_d1)
    result_fato, error_fato, problem_type = validate_fato(df_captacao_analitico_d1, df_captacao_analitico_d2, df_fato_d1, df_fato_d2)

    if(result_fato == True & result_group == True & refresh == 1):
        refresh_dataset('COMERCIAL')
    else:
        msg = "Bases não validadas. Erros encontrados." + "\n\nTipo de Problema: \n (   ) Tipo 1 - Preciso resolver\n ( X ) Tipo 2 - " + random.choice(phrases) if problem_type == 2 else ""

        print_info("Bases não validadas. Erros encontrados.", "warning")
        notify(("Bases não validadas. Erros encontrados." +
        "\n\nTipo de Problema: "
        "\n (    ) Tipo 1 - Preciso resolver"
        "\n ( X ) Tipo 2 - " + random.choice(phrases)
        if problem_type == 2 else ""), "warning")

    
def validate_fato(base1_d1, base1_d2, base2_d1, base2_d2):
    log.info("Method Validate Fato")
    print("Validação Fato")
    b1_ev_ins = base1_d1['INSCRITOS'] - base1_d2['INSCRITOS']
    b1_ev_apr = base1_d1['APROVADOS'] - base1_d2['APROVADOS']
    b1_ev_mat = base1_d1['MATRICULADOS'] - base1_d2['MATRICULADOS']
    
    b2_ev_ins = base2_d1['INSCRITOS'] - base2_d2['INSCRITOS']
    b2_ev_apr = base2_d1['APROVADOS'] - base2_d2['APROVADOS']
    b2_ev_mat = base2_d1['MATRICULADOS'] - base2_d2['MATRICULADOS']

    data = [
            ['EAD'
            ,str(b1_ev_ins[0]) + '|' + str(b2_ev_ins[0])
            ,str(b1_ev_apr[0]) + '|' + str(b2_ev_apr[0])
            ,str(b1_ev_mat[0]) + '|' + str(b2_ev_mat[0])
            ]
            , 
            ['PRE'
            ,str(b1_ev_ins[1]) + '|' + str(b2_ev_ins[1])
            ,str(b1_ev_apr[1]) + '|' + str(b2_ev_apr[1])
            ,str(b1_ev_mat[1]) + '|' + str(b2_ev_mat[1])
            ]
    ]

    df_new = pd.DataFrame(data, columns = ['MODALIDADE', 'INS', 'APR', 'MAT'])

    print(df_new)
    log.info(df_new)
    
    isValidate = True
    error = ''
    problem_type = 1

    #Checking quantity
    if not (b1_ev_ins[0] == b2_ev_ins[0]):
        isValidate = False
        error = error + '\n> error 1701 - Evolução inscritos EAD divergente: ' + str(b1_ev_ins[0]) + '|' + str(b2_ev_ins[0])
    if not (b1_ev_apr[0] == b2_ev_apr[0]):
        isValidate = False
        error = error + '\n> error 1702 - Evolução aprovados EAD divergente: ' + str(b1_ev_apr[0]) + '|' + str(b2_ev_apr[0])
    if not (b1_ev_mat[0] == b2_ev_mat[0]):
        isValidate = False
        error = error + '\n> error 1703 - Evolução Matriculados EAD divergente ' + str(b1_ev_mat[0]) + '|' + str(b2_ev_mat[0])
    if not (b1_ev_ins[1] == b2_ev_ins[1]):
        isValidate = False
        error = error + '\n> error 1704 - Evolução inscritos PRE divergente ' + str(b1_ev_ins[1]) + '|' + str(b2_ev_ins[1])
    if not (b1_ev_apr[1] == b2_ev_apr[1]):
        isValidate = False
        error = error + '\n> error 1705 - Evolução aprovados PRE divergente ' + str(b1_ev_apr[1]) + '|' + str(b2_ev_apr[1])
    if not (b1_ev_mat[1] == b2_ev_mat[1]):
        isValidate = False
        error = error + '\n> error 1706 - Evolução Matriculados PRE divergente ' + str(b1_ev_mat[1]) + '|' + str(b2_ev_mat[1])

    if(isValidate):
        problem_type = 2

    if(b2_ev_ins[0] < 0 or b2_ev_apr[0] < 0 or b2_ev_mat[0] < 0 or b2_ev_ins[1] < 0 or b2_ev_apr[1] < 0 or b2_ev_mat[1] < 0):
        isValidate = False
        error = error + '\n> ERROR 1707 - Evolução negativa'

    if(b2_ev_ins[0] == 0 or b2_ev_apr[0] == 0 or b2_ev_mat[0] == 0 or b2_ev_ins[0] == 0 or b2_ev_apr[0] == 0 or b2_ev_mat[0] == 0):
        isValidate = False
        error = error + '\n> ERROR 1708 - Evolução zerada'

    print("Fato: " + str(isValidate))
    log.error(error)
    print(error)

    if(isValidate):
        log.info("bases validadas")
        notify("Validação Fato: OK")
    else:
        log.error("erros ao validar bases")
        notify("Validação Fato: NOK " + error)

    return isValidate, error, problem_type

def validate_group(captacao_d1, group_graficos_d1):
    log.info("Method Validate Group")
    print('Validação Group Graficos D-1')
    
    data = [
            ['EAD'
            , str(captacao_d1['INSCRITOS'][0]) + '|' + str(group_graficos_d1['INSCRITOS'][0]) 
            , str(captacao_d1['APROVADOS'][0]) + '|' + str(group_graficos_d1['APROVADOS'][0])
            , str(captacao_d1['MATRICULADOS'][0]) + '|' + str(group_graficos_d1['MATRICULADOS'][0])
            ]
            , 
            ['PRE'
            ,str(captacao_d1['INSCRITOS'][1]) + '|' + str(group_graficos_d1['INSCRITOS'][1])
            ,str(captacao_d1['APROVADOS'][1]) + '|' + str(group_graficos_d1['APROVADOS'][1])
            ,str(captacao_d1['MATRICULADOS'][1]) + '|' + str(group_graficos_d1['MATRICULADOS'][1])
            ]
    ]

    df_new = pd.DataFrame(data, columns = ['MODALIDADE', 'INS', 'APR', 'MAT'])

    print(df_new)

    # print(captacao_d1)
    # print(group_graficos_d1)

    isValidate = True
    error = ''

    if(captacao_d1['INSCRITOS'][0] != group_graficos_d1['INSCRITOS'][0]):
        isValidate = False
        error = error + '| ERROR 1709 - Divergência Grupo Gráficos INSCRITOS EAD'

    if(captacao_d1['APROVADOS'][0] != group_graficos_d1['APROVADOS'][0]):
        isValidate = False
        error = error + '| ERROR 1710 - Divergência Grupo Gráficos APROVADOS EAD'

    if(captacao_d1['MATRICULADOS'][0] != group_graficos_d1['MATRICULADOS'][0]):
        isValidate = False
        error = error + '| ERROR 1711 - Divergência Grupo Gráficos MATRICULADOS EAD'

    if(captacao_d1['INSCRITOS'][1] != group_graficos_d1['INSCRITOS'][1]):
        isValidate = False
        error = error + '| ERROR 1712 - Divergência Grupo Gráficos INSCRITOS PRE'

    if(captacao_d1['APROVADOS'][1] != group_graficos_d1['APROVADOS'][1]):
        isValidate = False
        error = error + '| ERROR 1713 - Divergência Grupo Gráficos APROVADOS PRE'

    if(captacao_d1['MATRICULADOS'][1] != group_graficos_d1['MATRICULADOS'][1]):
        isValidate = False
        error = error + '| ERROR 1714 - Divergência Grupo Gráficos MATRICULADOS PRE'

    print("Group: " + str(isValidate))
    print(error)
    log.error(error)

    if(isValidate):
        log.info("bases validadas")
        notify("Validação Group: OK")
    else:
        log.error("erros ao validar bases")
        notify("Validação Group: NOK " + error)

    return isValidate, error

def get_datasets_in_group(ws_id = ''):
    try:
        log.info("Method Get Datasets in Group")
        print("Getting datasets in group")
        file_name = 'datasets_in_group.json'

        token = get_token(cfg.server['auth_method'])
        if not ws_id:
                df_groups = pd.read_csv("input/get_datasets_in_group.csv")
                ls_groups = df_groups['GROUP_ID'].tolist()
                for groupId in ls_groups:
                    print("Getting datasets for " + groupId)
                    url = 'https://api.powerbi.com/v1.0/myorg/groups/' + groupId + '/datasets'
                    file_name = 'datasets_in_group_' + groupId + '.json'
                    response = requests.get(url, headers={'Authorization' : 'Bearer ' + token})
                    save_json(response.content, file_name, cfg.paths['output'] + "Datasets In Group/")
        else:
            url = 'https://api.powerbi.com/v1.0/myorg/groups/' + ws_id + '/datasets'
            response = requests.get(url, headers={'Authorization' : 'Bearer ' + token})
            save_json(response.content, file_name, cfg.paths['output'] + "Datasets In Group/")
    except Exception as e:
        print(e)
        log.exception(e)

def get_token(auth_method):
    log.info("Method Get Token")
    print('Requesting token')
    authority_url = 'https://login.microsoftonline.com/common/'
    #authority_url = 'https://login.windows.net/common'
    resource_url = 'https://analysis.windows.net/powerbi/api'

    client_id = cfg.app["client_id"]
    client_secret = cfg.app["client_secret"]
    username = cfg.userinfo["username"]
    password = cfg.userinfo["password"]

    if (auth_method == 'ADAL'):
        print('Acquiring token from ADAL library\r')
        context = adal.AuthenticationContext(
        authority=authority_url,
        validate_authority=True,
        api_version=None
        )

        acquired_auth = context.acquire_token_with_username_password(
            resource=resource_url,
            client_id=client_id,
            username=username,
            password=password
        )
        # print('Response from ADAL \r')
        # print(acquired_auth)
        token = acquired_auth['accessToken']
    elif (auth_method == 'POST'):
        print('Acquiring token from POST method')
        data = {'grant_type':'password',
                'scope': 'openid',
                'resource': resource_url,
                'client_id': client_id,
                'client_secret':client_secret,
                'username': username,
                'password': password }

        acquired_auth = requests.post('https://login.microsoftonline.com/common/oauth2/token', data=data)
        acquired_auth_json = acquired_auth.json()

        print('Response from POST \r')
        # print(acquired_auth_json)
        token = acquired_auth_json['access_token']

    # if(cfg.dev['debug'] == '1'):
        # print('Token: ')
        # print(token)

    return token


def get_captacao_analitico(ciclo, data_index, conn):
    sql = "\
        SELECT \
            'CAPTACAO' as BASE, \
            DT_BASE,   \
            ds_modalidade as DS_MODALIDADE,   \
            sum(fl_inscrito) as INSCRITOS,    \
            sum(fl_aprovado) as APROVADOS,   \
            sum(fl_matriculado) as MATRICULADOS   \
        FROM TGT_BI_CAPTACAO_ANALITICO    \
        WHERE 1 = 1 \
            AND TO_CHAR(DT_BASE, 'YYYY-MM-DD HH24:MI:SS') in ('" + str(data_index) + "'  )  \
            AND DS_PERIODO_LETIVO = " + ciclo + "  \
            AND FL_EX_TOMBAMENTO = 0 \
            AND COALESCE(DS_TIPO_ALUNO,'x') NOT LIKE '%PROUNI%' \
        group by \
            DS_PERIODO_LETIVO, DT_BASE, DS_MODALIDADE \
        order by 3 asc \
    "

    df = system.get_oracle(sql, conn, 'captacao_analitico ' + str(data_index))
    return df

def get_group_graficos(ciclo, data_index, conn):
    sql = "\
        SELECT distinct  \
            'group' as BASE, \
            DT_DATA, \
            DS_MODALIDADE_CURSO AS DS_MODALIDADE , \
            sum(NUM_AGRUP_INSCRITOS) AS Inscritos, \
            sum(NUM_AGRUP_APROVADOS) AS Aprovados, \
            sum(NUM_AGRUP_MATRICULADOS) AS Matriculados \
        FROM SIGA_DESENV.TGT_BI_P33_GROUP_GRFCS_HST_" + ciclo[2:] + " tbpggh  \
        WHERE  \
            1 = 1 \
            AND TO_CHAR(DT_DATA, 'YYYY-MM-DD HH24:MI:SS') in ('" + str(data_index) + "'  )  \
            AND TP_TIPO_META IS NULL \
            AND FL_EX_TOMBAMENTO = 0 \
            AND TP_TIPO_ALUNO_META = 'EX-PROUNI' \
        GROUP BY DT_DATA ,TP_TIPO_META , DS_MODALIDADE_CURSO  \
        ORDER BY 3 asc \
    "

    df = system.get_oracle(sql, conn, 'group_graficos ' + str(data_index))
    return df

def get_fato(ciclo, data_index, conn):
    sql = "\
        SELECT  \
            DT_DATA \
            ,DS_MODALIDADE_CURSO AS DS_MODALIDADE \
            ,sum(FL_FLAG_INSCRITO) AS INSCRITOS \
            ,sum(FL_FLAG_APROVADO) AS APROVADOS \
            ,sum(FL_FLAG_MATRICULADO) AS MATRICULADOS \
        FROM  \
            SIGA_DESENV.TGT_BI_P33_FATO_CAP_RESUMO_" + ciclo[2:] + "   \
        WHERE  \
            1 = 1 \
            AND TO_CHAR(DT_DATA, 'YYYY-MM-DD HH24:MI:SS') in ('" + str(data_index) + "'  )  \
            AND FL_EX_TOMBAMENTO = 0 \
            AND TP_TIPO_ALUNO_META = 'EX-PROUNI' \
        GROUP BY  \
            DT_DATA  \
            ,DS_MODALIDADE_CURSO  \
        ORDER BY DS_MODALIDADE_CURSO \
    "

    df = system.get_oracle(sql, conn, 'fato_resumo ' + str(data_index) )
    return df

def get_datas_captacao(ciclo, conn):
    try:
        sql = "\
            SELECT DT_DATA FROM siga_desenv.TB_P33_ID_DATA_"+ciclo[2:]+" tpidt  WHERE ID_RANK = 2 \
        "

        df_data_d1 = system.get_oracle(sql, conn, 'datas rank 2')
        d1 = df_data_d1['DT_DATA'][0]

        sql = "\
            SELECT DT_DATA FROM siga_desenv.TB_P33_ID_DATA_"+ciclo[2:]+" tpidt  WHERE ID_RANK = 1 \
        "

        df_data_d2 = system.get_oracle(sql, conn, 'datas rank 1')
        d2 = df_data_d2['DT_DATA'][0]

        return d1, d2
    except Exception as Argument:
        log.exception("Matrix has found an error... Exiting")

def print_info(msg, log_type):
    if log_type == 'info':
        log.info(msg)
    elif log_type == 'warning':
        log.warning(msg)
    elif log_type == 'error':
        log.error(msg)
    else:
        log.info(msg)
