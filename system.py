import cx_Oracle
import pandas as pd
import sys
import time
import os
import collections
import settings

cfg = settings.Config

def get_args():
    arg_names = ['app','operation','id','param']
    args = dict(zip(arg_names, sys.argv))

    arg_list = collections.namedtuple('arg_list', arg_names)
    return arg_list(*(args.get(arg, None) for arg in arg_names))

def execute_process(process):
    print('\n')
    process.start()
    while process.is_alive():
        animated_loading()
    print('\n')
    if(cfg.dev['debug'] == '0'):
        os.system("mode con cols=115 lines=80")
        f = open("./_img/neoascii.txt", 'r')
        file_contents = f.read()
        print(file_contents)

def animated_loading():
    chars = "/—\|"
    for char in chars:
        sys.stdout.flush()
        sys.stdout.write('\r' + 'loading... ' + char)
        time.sleep(.1)
        sys.stdout.write('\r'+'Done              ')
        

def loading(msg='hi', status = 'done'):
    animation = "."
    if(msg):
        print(msg)

    while(True):
        animation = animation + '.'
        time.sleep(0.5)
        sys.stdout.write("\r" + animation)
        sys.stdout.flush()

    print("\n")

def defined_loading(n):
    for i in range(n):
        time.sleep(1)
        sys.stdout.write('\r'+'loading...  process '+str(i)+'/'+str(n)+' '+ '{:.2f}'.format(i/n*100)+'%')
        sys.stdout.flush()
    sys.stdout.write('\r'+'loading... finished               \n')

def connect_oracle():
    hostname = cfg.db["hostname"]
    portnumber = cfg.db["portnumber"]
    servicename = cfg.db["servicename"]
    username = cfg.db["username"]
    pwd = cfg.db["pwd"]

    dsn_tns = cx_Oracle.makedsn(hostname, portnumber, service_name=servicename)
    conn = cx_Oracle.connect(user=username, password=pwd, dsn=dsn_tns)
    print("oracle conn ok")
    return conn

def close_oracle(conn):
    conn.close()
    print("--- conn close ---")

def get_oracle(sql, conn, id):
    df = pd.read_sql(sql, conn)
    print (id + ' ok')
    return df