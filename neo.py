import logging
import threading
import system
import application as app
import settings

def main():
	cfg = settings.Config

	print("Entering Matrix")
	logging.info("")
	logging.info("---------------- Entering Matrix ----------------")
	args = system.get_args()

	if not args.operation:
		print("No operation selected. You have to make your choice. Whether the blue or red one\nLeaving Matrix")
		logging.info("No operation selected. You have to make your choice. Whether the blue or red one")
		logging.info("################## Exiting Matrix ##################")
		exit()

	try:
		if(args.operation == 'test'):
			print('Testing function')
			logging.info('Testing Function')
			process = threading.Thread(name='process', target=app.test, args=(args.id,args.param))
			system.execute_process(process)

		elif(args.operation == 'get_refreshables'):
			print('Getting refreshables information')
			logging.info('Getting refreshables information')
			process = threading.Thread(name='process', target=app.get_refreshables, args=())
			system.execute_process(process)

		elif(args.operation == 'get_group_users'):
			print('Getting all premium group users')
			logging.info('Getting all premium group users')
			process = threading.Thread(name='process', target=app.get_group_users, args=())
			system.execute_process(process)

		elif(args.operation == 'refresh_dataset'):
			print('Triggering refresh dataset')
			logging.info('Triggering refresh dataset')
			if not args.id:
				print("Dataset missing. Exiting process")
				logging.warning("Dataset missing. Exiting process")
				exit()
			process = threading.Thread(name='process', target=app.refresh_dataset, args=(args.id,args.param))
			system.execute_process(process)

		elif(args.operation == 'delete_workspace'):
			print('Removing Workspace ')
			logging.info('Removing Workspace')
			process = threading.Thread(name='process', target=app.delete_workspace, args=(args.id,))
			system.execute_process(process)

		elif(args.operation == 'delete_report_dataset'):
			print('Removing dataset ')
			logging.info('Removing dataset')
			process = threading.Thread(name='process', target=app.delete_report_dataset, args=(args.id,))
			system.execute_process(process)

		elif(args.operation == 'validate'):
			print('Validating 33 dataset')
			logging.info('Validating 33 dataset')
			process = threading.Thread(name='process', target=app.check_databases, args=())
			system.execute_process(process)

		elif(args.operation == 'get_datasets_in_group'):
			print('Getting Datasets in group')
			logging.info('Getting datasets in group')
			process = threading.Thread(name='process', target=app.get_datasets_in_group, args=(args.id,))
			system.execute_process(process)

		else:
			print("Invalid operation\nYou've choosen the blue pill... You'll be taken out of the Matrix")
			logging.warning("Invalid operation\nYou've choosen the blue pill... You'll be taken out of the Matrix")
			exit()

		print("Your Job is Done. Exiting Matrix")
		logging.info("################## Your Job is Done. Exiting Matrix ##################")
		logging.info("")

	except Exception as e:
		logging.exception("Matrix has found an error... Exiting")
		logging.exception(e)

if __name__ == "__main__":
    main()