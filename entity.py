from enum import Enum

class Dataset(Enum):
    CORPORATIVO = 'c56b35cf-ef53-4a51-9fd5-18933d86a666'
    COMERCIAL = '11e1fca9-4745-482c-b3eb-9df655b93111'
    NPS_HOMOL = 'cb6734ea-c37c-4a16-9cdc-fda5d0075282'
    NPS_PROD = '8d8d4bbe-7d82-42e3-8aac-ca043ab9a97a'

class Workspace(Enum):
    PBI_OFICIAL = '46ac7ff6-0683-4ad0-96c8-ac4b8997d56a'